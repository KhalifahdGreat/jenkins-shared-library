#!/usr/bin/env groovy

def call() {
    echo 'deploying the application'
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER' )]) {
        sh 'docker build -t kalidgreat/my-node-app:jma-2.0 .'
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh 'docker push kalidgreat/my-node-app:jma-2.0'
    }
}
